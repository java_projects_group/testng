package ParallelExecution_Issues;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


final class Worker {
    public static final String name =
            Thread.currentThread().getName() + ":" + Thread.currentThread().getId();


    @Override
    public String toString() {
        return String.format("Worker{%s}", name);
    }
}

public class ParallelRunSuite {
    private final static Worker worker = new Worker();
    private final static ThreadLocal<Worker> localWorker = ThreadLocal.withInitial(() -> new Worker());

    @BeforeMethod
    public void beforeMethod() {
        long id = Thread.currentThread().getId();
        System.out.println("Before test-method. Thread id is: " + id);
    }

    @Test
    public void testMethod1() {
        //long id = Thread.currentThread().getId();
        System.out.println("testMethod1: " + worker);
        System.out.println("testMethod1: " + localWorker.get());
    }

    @Test
    public void testMethod2() {
        long id = Thread.currentThread().getId();
        System.out.println("testMethod2: " + worker);
        System.out.println("testMethod2: " + localWorker.get());
    }

    @Test
    public void testMethod3() {
        long id = Thread.currentThread().getId();
        System.out.println("testMethod3: " + worker);
        System.out.println("testMethod3: " + localWorker.get());
    }


    @AfterMethod
    public void afterMethod() {
        long id = Thread.currentThread().getId();
        System.out.println("After test-method. Thread id is: " + id);
    }
}
